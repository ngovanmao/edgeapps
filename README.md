# Introduction
For evaluating [our hierarchical edge computing (HEC) system](https://gitlab.com/ngovanmao/edgecomputing), we developed three offloaded services to be the services for offloading image processing tasks, and package each into a Docker container:
  * A face recognition service based on Openface [20],
  * An object recognition service based on Yolo [21], and
  * A Simple service which is a dumb TCP server that simply responds to each incoming offloading request with an incrementing counter (and hence the processing delay is treated as zero).

**For more details, please read our paper:** [Globecom'20] Mao V. Ngo, Tie Luo, Hieu T. Hoang, and Tony Q.S. Quek, "Coordinated Container Migration and Base Station Handover in Mobile Edge Computing," IEEE Global Communications Conference (GLOBECOM), December 2020.  [PDF](https://arxiv.org/abs/2009.05682), [Video](https://youtu.be/IqsHe43lHaw)

# Citation
Please cite EdgeComputing in your publications if it helps your research:
```
@proceeding{MaoGlobecom2020,
  title="{Coordinated Container Migration and Base Station Handover in Mobile Edge Computing}",
  author={Mao~V.~Ngo and Tie~Luo and Hieu~T.~Hoang and Tony~Q.~S.~Quek},
  booktitle={Proc. IEEE GLOBECOM},
  pages={},
  year={2020},
  month={Dec.},
  address={Taiwan}
}
```
