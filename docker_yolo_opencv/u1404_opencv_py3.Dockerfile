FROM ubuntu:14.04

MAINTAINER Ngo Van Mao newngovanmao@gmail.com

# install
RUN apt-get update && apt-get install -y \
    build-essential \
    python3-dev \
    wget \
    git \
    cmake \
    python3-numpy \
    g++ \
    gcc \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /
ENV OPENCV_VERSION="3.4.2"
RUN wget https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.tar.gz -O ocv.tar.gz\
&& tar xf ocv.tar.gz \
&& cd opencv-${OPENCV_VERSION} \
&& mkdir release \
&& cd release \
&& cmake -DBUILD_TIFF=ON \
  -DBUILD_opencv_java=OFF \
  -DWITH_CUDA=OFF \
  -DWITH_OPENGL=ON \
  -DWITH_OPENCL=ON \
  -DWITH_IPP=ON \
  -DWITH_TBB=ON \
  -DWITH_EIGEN=ON \
  -DWITH_V4L=ON \
  -DBUILD_TESTS=OFF \
  -DBUILD_PERF_TESTS=OFF \
  -DCMAKE_BUILD_TYPE=RELEASE \
  -D CMAKE_INSTALL_PREFIX=/usr/local \
  -D BUILD_PYTHON_SUPPORT=ON \
  -D PYTHON_EXECUTABLE=/usr/bin/python3 \
  .. \
&& make -j8 \
&& make install \
&& rm /ocv.tar.gz \
&& rm -rf /opencv-${OPENCV_VERSION}
