import cv2 as cv
import argparse
import numpy as np
import json
import socket
import time
import threading
from struct import unpack

# Initialize the parameters
confThreshold = 0.5  #Confidence threshold
nmsThreshold = 0.4   #Non-maximum suppression threshold
inpWidth = 416       #Width of network's input image
inpHeight = 416      #Height of network's input image
i = 0

def analyze_send_back_result(conn, bdata, transfer_time):
    return_msg = process_image_data(bdata, transfer_time)
    payload = (json.dumps(return_msg) + "\n").encode('utf-8')
    conn.sendall(payload)
    # conn.send((return_msg + '\n').encode())
    if args.verbose:
        print("Done processing {}".format(return_msg))

# Get the names of the output layers
def getOutputsNames(net):
    # Get the names of all the layers in the network
    layersNames = net.getLayerNames()
    # Get the names of the output layers, i.e. the layers with unconnected outputs
    return [layersNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]

# Remove the bounding boxes with low confidence using non-maxima suppression
def postprocess(frame, outs):
    frameHeight = frame.shape[0]
    frameWidth = frame.shape[1]
    # Scan through all the bounding boxes output from the network and keep only the
    # ones with high confidence scores. Assign the box's class label as the class with the highest score.
    classIds = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            if confidence > confThreshold:
                center_x = int(detection[0] * frameWidth)
                center_y = int(detection[1] * frameHeight)
                width = int(detection[2] * frameWidth)
                height = int(detection[3] * frameHeight)
                left = int(center_x - width / 2)
                top = int(center_y - height / 2)
                classIds.append(classId)
                confidences.append(float(confidence))
                boxes.append([left, top, width, height])
    # Perform non maximum suppression to eliminate redundant overlapping boxes with
    # lower confidences.
    indices = cv.dnn.NMSBoxes(boxes, confidences, confThreshold, nmsThreshold)
    res = []
    for i in indices:
        i = i[0]
        box = boxes[i]
        left = box[0]
        top = box[1]
        width = box[2]
        height = box[3]
        #print(classIds[i], confidences[i], left, top, left + width, top + height)
        res.append({'object':str(classes[classIds[i]]), 'confidence':confidences[i],\
            'bb':[left, top, left + width, top + height]})
    #print(res)
    return res

def process_image_data(bdata, transfer_time):
    s_proc_time = time.time()
    nparr = np.frombuffer(bdata, np.uint8)
    frame = cv.imdecode(nparr, cv.IMREAD_COLOR)
    return process_frame_image(frame, s_proc_time, transfer_time)

def process_image_file(fname, transfer_time):
    s_proc_time = time.time()
    # Process inputs
    s = time.time()
    cap = cv.VideoCapture(fname)
    # get frame from the video
    hasFrame, frame = cap.read()
    if args.verbose:
        print("read file = {}".format(time.time() - s))
    # Stop the program if reached end of video
    if not hasFrame:
        return
    return process_frame_image(frame, s_proc_time, transfer_time)

def process_frame_image(frame, s_proc_time, transfer_time):
    # Create a 4D blob from a frame.
    blob = cv.dnn.blobFromImage(frame, 1/255, (inpWidth, inpHeight),
        [0,0,0], 1, crop=False)
    # Sets the input to the network
    net.setInput(blob)
    # Runs the forward pass to get output of the output layers
    outs = net.forward(getOutputsNames(net))
    # Remove the bounding boxes with low confidence
    r = postprocess(frame, outs)
    # Put efficiency information. The function getPerfProfile returns
    # the overall time for inference(t) and the timings for each of
    # the layers(in layersTimes)
    #t, _ = net.getPerfProfile()
    #infer_time = t * 1000.0 / cv.getTickFrequency() # ms
    #print('Inference time: %.2f ms' % (infer_time))
    str_r = {'general': {'indexServer':i,
                        'transferTime[ms]':transfer_time * 1000.0,
                        'processTime[ms]':(time.time()-s_proc_time)*1000.0},
              'list':r}
    #_msg = '{}'.format(str_r)
    return str_r

def client_thread(conn, ip, port):
    global i
    print("[+] New Thread started for a new connection from {}:{}\n".format(ip, port))
    socket_opened = True
    length = 0
    start_time = 0
    data = b''
    buf = b''
    while socket_opened:
        if length == 0:
            if len(buf) < 4:
                buf += conn.recv(4-len(buf))
                if not buf:
                    print("socket is closed")
                    socket_opened = False
                    buf = b''
            else:
                start_time = time.time()
                (length,) = unpack('!i', buf)
                i += 1
                buf = b''
                data = b''
        elif len(data) < length:
            to_read = length - len(data)
            tem_buf = conn.recv(4096 if to_read > 4096 else to_read)
            if not tem_buf:
                print("socket is closed")
                socket_opened = False
                data = b''
                buf = b''
                length = 0
            data += tem_buf
        else:
            if args.verbose:
                print("received len(data)={}".format(length))
            transfer_time = time.time() - start_time
            analyze_send_back_result(conn, data, transfer_time)
            #inferring_thread = threading.Thread(target=analyze_send_back_result,\
            #                            args=(conn, data, transfer_time))
            #inferring_thread.setDaemon(True)
            #inferring_thread.start()
            length = 0
            data = b''
            buf = b''
    print("[-] close a socket")
    conn.shutdown(socket.SHUT_RDWR)
    conn.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Server object detection using YOLO in OENCV')
    parser.add_argument(
        '--config',
        type=str,
        help='Path to a config file in cfg folder',
        default='yolov3.cfg')
    parser.add_argument(
        '--weights',
        type=str,
        help='Path to a weights file for darknet model',
        default='yolov3.weights')
    parser.add_argument(
        '--img',
        type=str,
        help='Path to requesting image.',
        default='bird.jpg')
    parser.add_argument(
        '--port',
        type=int,
        help='The opening port number of the server.',
        default=9988)
    parser.add_argument('--verbose', action='store_true')
    args = parser.parse_args()

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind(('', args.port))
    # serve upto 16 users
    server.listen(16)
    global net, classes, args
    # Load names of classes
    classesFile = "coco.names";
    classes = None
    with open(classesFile, 'rt') as f:
        classes = f.read().rstrip('\n').split('\n')

    # Give the configuration and weight files for the model and load the network using them.
    net = cv.dnn.readNetFromDarknet(args.config, args.weights)
    net.setPreferableBackend(cv.dnn.DNN_BACKEND_OPENCV)
    net.setPreferableTarget(cv.dnn.DNN_TARGET_CPU)
    '''
    with open(args.img, 'rb') as imgF:
        f = imgF.read()
        b = bytearray(f)
        #b = f
    process_image_data(b, 0)
    process_image_file(args.img, 0)
    '''
    while True:
        print("Multithreaded Python server: waiting for connections from TCP clients. Port {}".format(args.port))
        try:
            (conn, (ip, port)) = server.accept()
            threading.Thread(target=client_thread, args=(conn, ip, port)).start()
            print("current number of threads {}".format(threading.active_count()))
        except socket.error:
            print("socket is not closed properly.")
