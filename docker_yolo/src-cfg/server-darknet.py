from ctypes import *
import math
import random
import logging

import socket
import os
import time
import threading
import argparse
from struct import unpack

fileDir = os.path.dirname(os.path.realpath(__file__))
darknetDir = fileDir
cfgDir = os.path.join(darknetDir, 'cfg')

def sample(probs):
    s = sum(probs)
    probs = [a/s for a in probs]
    r = random.uniform(0, 1)
    for i in range(len(probs)):
        r = r - probs[i]
        if r <= 0:
            return i
    return len(probs)-1

def c_array(ctype, values):
    arr = (ctype*len(values))()
    arr[:] = values
    return arr

class BOX(Structure):
    _fields_ = [("x", c_float),
                ("y", c_float),
                ("w", c_float),
                ("h", c_float)]

class DETECTION(Structure):
    _fields_ = [("bbox", BOX),
                ("classes", c_int),
                ("prob", POINTER(c_float)),
                ("mask", POINTER(c_float)),
                ("objectness", c_float),
                ("sort_class", c_int)]


class IMAGE(Structure):
    _fields_ = [("w", c_int),
                ("h", c_int),
                ("c", c_int),
                ("data", POINTER(c_float))]

class METADATA(Structure):
    _fields_ = [("classes", c_int),
                ("names", POINTER(c_char_p))]



#lib = CDLL("/home/pjreddie/documents/darknet/libdarknet.so", RTLD_GLOBAL)
lib = CDLL(os.path.join(darknetDir, "libdarknet.so"), RTLD_GLOBAL)
lib.network_width.argtypes = [c_void_p]
lib.network_width.restype = c_int
lib.network_height.argtypes = [c_void_p]
lib.network_height.restype = c_int

predict = lib.network_predict
predict.argtypes = [c_void_p, POINTER(c_float)]
predict.restype = POINTER(c_float)

set_gpu = lib.cuda_set_device
set_gpu.argtypes = [c_int]

make_image = lib.make_image
make_image.argtypes = [c_int, c_int, c_int]
make_image.restype = IMAGE

get_network_boxes = lib.get_network_boxes
get_network_boxes.argtypes = [c_void_p, c_int, c_int, c_float, c_float, POINTER(c_int), c_int, POINTER(c_int)]
get_network_boxes.restype = POINTER(DETECTION)

make_network_boxes = lib.make_network_boxes
make_network_boxes.argtypes = [c_void_p]
make_network_boxes.restype = POINTER(DETECTION)

free_detections = lib.free_detections
free_detections.argtypes = [POINTER(DETECTION), c_int]

free_ptrs = lib.free_ptrs
free_ptrs.argtypes = [POINTER(c_void_p), c_int]

network_predict = lib.network_predict
network_predict.argtypes = [c_void_p, POINTER(c_float)]

reset_rnn = lib.reset_rnn
reset_rnn.argtypes = [c_void_p]

load_net = lib.load_network
load_net.argtypes = [c_char_p, c_char_p, c_int]
load_net.restype = c_void_p

do_nms_obj = lib.do_nms_obj
do_nms_obj.argtypes = [POINTER(DETECTION), c_int, c_int, c_float]

do_nms_sort = lib.do_nms_sort
do_nms_sort.argtypes = [POINTER(DETECTION), c_int, c_int, c_float]

free_image = lib.free_image
free_image.argtypes = [IMAGE]

letterbox_image = lib.letterbox_image
letterbox_image.argtypes = [IMAGE, c_int, c_int]
letterbox_image.restype = IMAGE

load_meta = lib.get_metadata
lib.get_metadata.argtypes = [c_char_p]
lib.get_metadata.restype = METADATA

load_image = lib.load_image_color
load_image.argtypes = [c_char_p, c_int, c_int]
load_image.restype = IMAGE

rgbgr_image = lib.rgbgr_image
rgbgr_image.argtypes = [IMAGE]

predict_image = lib.network_predict_image
predict_image.argtypes = [c_void_p, IMAGE]
predict_image.restype = POINTER(c_float)

def classify(net, meta, im):
    out = predict_image(net, im)
    res = []
    for i in range(meta.classes):
        res.append((meta.names[i], out[i]))
    res = sorted(res, key=lambda x: -x[1])
    return res

def detect(net, meta, image, thresh=.5, hier_thresh=.5, nms=.45):
    im = load_image(image, 0, 0)
    num = c_int(0)
    pnum = pointer(num)
    predict_image(net, im)
    dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, None, 0, pnum)
    num = pnum[0]
    if (nms): do_nms_obj(dets, num, meta.classes, nms);

    res = []
    for j in range(num):
        for i in range(meta.classes):
            if dets[j].prob[i] > 0:
                b = dets[j].bbox
                res.append({'object':str(meta.names[i]), 'confidence':dets[j].prob[i],\
                    'bb':[b.x - b.w/2, b.y - b.h/2, b.x + b.w/2, b.y + b.h/2]})
    #res = sorted(res, key=lambda x: -x[1])
    free_image(im)
    free_detections(dets, num)
    return res


i = 0
def analyze_send_back_result(conn, fname, transfer_time):
    start_time = time.time()
    r = detect(net, meta, fname)
    str_r = '{\'general\':{\'indexServer\':%s,\'transferTime\':%s,\'processTime\':%s},\'list\':'\
            %(str(i), str(transfer_time), str(time.time() - start_time)) + str(r) + '}'
    # Debug only
    logging.debug(str_r)
    conn.send(str_r + '\n' )
    # comment out the line below if want to see the received images.
    os.remove(fname)
    logging.debug("[-] close thread of processing")


def client_thread(conn, ip, port):
    global i
    print("[+] New Thread started for a new connection from {}:{}\n".format(ip, str(port)))
    socket_opened = True
    """TODO: this is temporary solution to hack the client close socket
    during sending back the result. I think refactor the code into class threading.
    Inside the class, we can call anther thread for sending back result, and have a class
    variable to check the conn is alive or not (e.g., conn.send() == 0 is connection broken)
    """
    #conn.settimeout(1)
    while socket_opened:
        buf = ''
        while len(buf) < 4:
            tem_buf = conn.recv(4-len(buf))
            if not tem_buf:
                print("socket is closed")
                socket_opened = False
                break
            buf += tem_buf
        if not socket_opened: break
        start_time = time.time()
        (length,) = unpack('!i', buf)
        i += 1
        fname = str(i) + '.jpg'
        fp = open(fname, 'wb')
        data = b''
        while len(data) < length:
            to_read = length - len(data)
            tem_data = conn.recv(
                4096 if to_read > 4096 else to_read)
            if not tem_data:
                print("socket is closed no data anymore.")
                socket_opened = False
                break
            data += tem_data
        if not socket_opened: break
        fp.write(data)
        fp.close()
        transfer_time = time.time() - start_time
        inferring_thread = threading.Thread(target=analyze_send_back_result,\
                                    args=(conn, fname, transfer_time))
        inferring_thread.setDaemon(True)
        inferring_thread.start()
    print("[-] close a socket")
    conn.shutdown(socket.SHUT_RDWR)
    conn.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--config',
        type=str,
        help='Path to a config file in cfg folder',
        default=os.path.join(cfgDir, 'yolov3.cfg'))

    parser.add_argument(
        '--weights',
        type=str,
        help='Path to a weights file for darknet model',
        default=os.path.join(darknetDir, 'yolov3.weights'))

    parser.add_argument(
        '--metadata',
        type=str,
        help='Path to meta data file.',
        default=os.path.join(cfgDir, 'coco.data'))

    parser.add_argument(
        '--img',
        type=str,
        help='Path to requesting image.',
        default=os.path.join(darknetDir, 'data/dog.jpg'))

    parser.add_argument(
        '--port',
        type=int,
        help='The opening port number of the server.',
        default=9988)
    parser.add_argument('--verbose', action='store_true')

    global args
    args = parser.parse_args()

    FORMAT = '%(asctime)-15s %(message)s'
    if args.verbose:
        logging.basicConfig(format=FORMAT, level=logging.DEBUG)
    else:
        logging.basicConfig(format=FORMAT, level=logging.INFO)

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind(('', args.port))
    # serve upto 16 users
    server.listen(16)
    global net, meta
    net = load_net(args.config, args.weights, 0)
    meta = load_meta(args.metadata)
    while True:
        logging.info("Multithreaded Python server: waiting for connections from TCP clients. Port {}".format(args.port))
        try:
            (conn, (ip, port)) = server.accept()
            threading.Thread(target=client_thread, args=(conn, ip, port)).start()
            logging.info("current number of threads {}".format(threading.active_count()))
        except socket.error:
            logging.error("socket is not closed properly.")
