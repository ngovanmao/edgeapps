#!/bin/bash
# Copyright (2018) Ngo Van Mao, email: newngovanmao@gmail.com

# Get the docker name as the first argument, otherwise set as latest.
if [ -z $1 ]; then
    DOCKER_NAME=ngovanmao/yolov3-gpu-arm64v8:01 #latest
else    
    DOCKER_NAME=$1
fi    

# copy source files to this place
cp -r ../../../src-cfg/* .

# build docker
sudo tx2-docker build -f Dockerfile -t $DOCKER_NAME .

# clean up
rm -rf cfg/ data/ yolov3.weights server-darknet.py
