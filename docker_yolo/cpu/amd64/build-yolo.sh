#!/bin/bash
# Copyright (2018) Ngo Van Mao, email: newngovanmao@gmail.com

# Get the docker name as the first argument, otherwise set as latest.
DOCKER_NAME_LATEST=ngovanmao/yolov3-mini-cpu-amd64:latest
if [ -z $1 ]; then
    DOCKER_NAME=$DOCKER_NAME_LATEST
else    
    DOCKER_NAME=$1
fi    

# copy source files to this place
cp -r ../../src-cfg/* .

# build docker
sudo docker build -f Dockerfile -t $DOCKER_NAME -t $DOCKER_NAME_LATEST .

# clean up
rm -rf cfg/ data/ yolov3.weights yolov3-tiny.weights server-darknet.py
