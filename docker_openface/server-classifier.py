#!/usr/bin/env python2
#
# This is a server-classifier file. Partially inherit from classifier.py file,
# And introduce TCP socker server handler to receiving image requests from
# clients.
# Ngo Van Mao
# 2017/12/08
#
# Copyright 2015-2016 Carnegie Mellon University
# Copyright 2017 Singapore University of Technology and Design
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from __future__ import division
import time
import socket
from threading import Thread
import argparse
import cv2
import os
import pickle
import sys
from struct import unpack
import numpy as np
np.set_printoptions(precision=2)
import openface
from sklearn.mixture import GMM

fileDir = os.path.dirname(os.path.realpath(__file__))
modelDir = os.path.join(fileDir, 'models')
dlibModelDir = os.path.join(modelDir, 'dlib')
openfaceModelDir = os.path.join(modelDir, 'openface')
i = 0

def getRep(imgPath, multiple=False):
    start = time.time()
    bgrImg = cv2.imread(imgPath)
    if bgrImg is None:
        raise Exception("Unable to load image: {}".format(imgPath))
    rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)
    if args.verbose:
        print("  + Original size: {}".format(rgbImg.shape))
        print("Loading the image took {} seconds.".format(time.time() - start))
    return getRepFrame(rgbImg, multiple)

def getRepFrame(rgbImg, multiple=False):
    start = time.time()
    if multiple:
        bbs = align.getAllFaceBoundingBoxes(rgbImg)
    else:
        bb1 = align.getLargestFaceBoundingBox(rgbImg)
        bbs = [bb1]
    if len(bbs) == 0 or (not multiple and bb1 is None):
        print("Unable to find a face")
        return []
    if args.verbose:
        print("Face detection took {} seconds.".format(time.time() - start))
    reps = []
    for bb in bbs:
        start = time.time()
        alignedFace = align.align(
            args.imgDim,
            rgbImg,
            bb,
            landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
        if alignedFace is None:
            raise Exception("Unable to align image")
        if args.verbose:
            print("Alignment took {} seconds.".format(time.time() - start))
            print("This bbox is centered at {}, {}".
                format(bb.center().x, bb.center().y))
        start = time.time()
        rep = net.forward(alignedFace)
        if args.verbose:
            print("Neural network forward pass took {} seconds.".format(
                time.time() - start))
        reps.append((bb, rep))
    sreps = sorted(reps, key=lambda x: x[0])
    return sreps

def scale_img(img):
    max_h = 300
    max_w = 300
    ori_h, ori_w, ori_channels = img.shape
    if ori_h > max_h or ori_w > max_w:
        scale = max_h / float(ori_h)
        if max_w / float(ori_w) < scale:
            scale = max_w /float(ori_w)
        small_img = cv2.resize(img, None, fx=scale, fy=scale)
    else:
        small_img = img
        scale = 1.0
    return small_img, scale

def infer(data):
    results = []
    multiple = args.multi
    nparr = np.frombuffer(data, np.uint8)
    rgbImg = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    small_img, scale = scale_img(rgbImg)
    reps = getRepFrame(small_img, multiple)
    for r in reps:
        rep = r[1].reshape(1, -1)
        bb = r[0]
        start = time.time()
        predictions = clf.predict_proba(rep).ravel()
        maxI = np.argmax(predictions)
        person = le.inverse_transform(maxI)
        confidence = predictions[maxI]
        if args.verbose:
            print("Prediction took {} seconds.".format(time.time() - start))
        results.append({'object':str(person.decode('utf-8')),
                        'bb':['{}'.format(bb.left()/scale),
                              '{}'.format(bb.top()/scale),
                              '{}'.format(bb.right()/scale),
                              '{}'.format(bb.bottom()/scale)],
                        'confidence':confidence})
        if isinstance(clf, GMM):
            dist = np.linalg.norm(rep - clf.means_[maxI])
            print("  + Distance from the mean: {}".format(dist))
    if args.verbose:
        print(results)
    return results

def analyze_send_back_result(conn, data, transfer_time):
    start_time = time.time()
    r = infer(data)
    str_r = {'general': {'indexServer':i,
                        'transferTime[ms]':transfer_time * 1000.0,
                        'processTime[ms]':(time.time() - start_time) * 1000.0},
              'list':r}
    return_msg = '{}'.format(str_r)
    conn.send(return_msg + '\n' )

def client_thread(conn, ip, port):
    global i
    if args.verbose:
        print("[+] New Thread started for a connection from {}:{}\n".format(ip, port))
    socket_opened = True
    length = 0
    start_time = 0
    data = b''
    buf = b''
    while socket_opened:
        if length == 0:
            if len(buf) < 4:
                buf += conn.recv(4-len(buf))
                if not buf:
                    print("socket is closed")
                    socket_opened = False
                    buf = b''
            else:
                start_time = time.time()
                (length,) = unpack('!i', buf)
                i += 1
                buf = b''
                data = b''
        elif len(data) < length:
            to_read = length - len(data)
            tem_buf = conn.recv(4096 if to_read > 4096 else to_read)
            if not tem_buf:
                print("socket is closed")
                socket_opened = False
                data = b''
                length = 0
            data += tem_buf
        else:
            if args.verbose:
                print("received len(data)={}".format(length))
            transfer_time = time.time() - start_time
            inferring_thread = Thread(target=analyze_send_back_result,
                args=(conn, data, transfer_time))
            inferring_thread.setDaemon(True)
            inferring_thread.start()
            length = 0
            data = b''
            buf = b''
    conn.shutdown(socket.SHUT_RDWR)
    print("[-] close a socket")
    conn.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--dlibFacePredictor',
        type=str,
        help="Path to dlib's face predictor.",
        default=os.path.join(
            dlibModelDir,
            "shape_predictor_68_face_landmarks.dat"))
    parser.add_argument(
        '--networkModel',
        type=str,
        help="Path to Torch network model.",
        default=os.path.join(
            openfaceModelDir,
            'nn4.small2.v1.t7'))
    parser.add_argument('--imgDim',
        type=int,
        help="Default image dimension.",
        default=96)
    parser.add_argument('--cuda', action='store_true')
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument(
        '--port',
        type=int,
        help="Listening port of Openface service.",
        default=9999)
    parser.add_argument(
        'classifierModel',
        type=str,
        help='The Python pickle representing the classifier. This is NOT the Torch network model, which can be set with --networkModel.')
    parser.add_argument('--multi',
        help="Infer multiple faces in image",
        action="store_true")

    global args, le, clf
    args = parser.parse_args()
    start = time.time()
    align = openface.AlignDlib(args.dlibFacePredictor)
    net = openface.TorchNeuralNet(args.networkModel, imgDim=args.imgDim,
                                  cuda=args.cuda)
    if args.verbose:
        print("Loading the dlib and OpenFace models took {} seconds.".format(
            time.time() - start))
        start = time.time()

    with open(args.classifierModel, 'rb') as f:
        if sys.version_info[0] < 3:
            (le, clf) = pickle.load(f)
        else:
            (le, clf) = pickle.load(f, encoding='latin1')

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind(("", args.port))
    server.listen(4)
    print("Multithreaded Python server with port= {}...".format(args.port))
    while True:
        (conn, (ip, port)) = server.accept()
        Thread(target=client_thread, args=(conn, ip, port)).start()
